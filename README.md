# robo_msg: A messaging package for ROS to send BeagleBone commands

This project features code for 2 different boards on different branches:
* jetson
* beaglebone

First make sure you have ros installed.
> Install ros on jetson nano https://www.stereolabs.com/blog/ros-and-nvidia-jetson-nano/

This package is seperate so we can create and use custom messages as needed tailored specifically towards communicating with our beagleboard blue.

To build this package from your catkin_ws run:
`catkin_make --only-pkg-with-deps robo_msg`

Note: ROS must first have its setup script sourced before packages can be run
`source ros_catkin_ws/devel/setup.sh`
> If you did not install ros from source run: `source /opt/ros/noetic/setup.bash`

Local talkers must have the ROS_IP exported to communicate properly with host's roscore
`export ROS_IP=<client_ip_address>`


Example command to run the subscriber executable:
`rosrun robo_msg subscriber`

To run the talker node:
`rosrun robo_msg talker.py`


The BeagleBone image should have a script 'setup_ros.h' at its root. This will set up the ros ip addresses according to the usb connection
source it with `source setup_ros.sh`


To copy files from this folder into the bb simply run:
`rsync -havz robo_msg/* debian@192.168.7.2:/home/debian/ros_catkin_ws/src/robo_msg/`
