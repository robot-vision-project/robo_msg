#include <string>

typedef enum {
  FORWARD,
  BACKWARD
} command;

std::string commandToString(command e) {
  switch (e) {
    case FORWARD:
      return "FORWARD";
      break;
    case BACKWARD:
      return "BACKWARD";
      break;
    default:
      return "INVALID";
  }
}
