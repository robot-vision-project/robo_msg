#!/usr/bin/env python
#Command line tool for sending robo_msg commands to the subscriber
import rospy
import time

from robo_msg.msg import Motor

def talker(command:int, speed:int):
    msg.time =time.time()
    print(msg.time)
    msg.command = command
    msg.speed = speed
    pub.publish(msg)
    rate.sleep()
    if(command < 0 or command > 3):
    	print("command invalid")
    else:
    	print("Command sent: {}, speed:{}".format(command,speed))
    
if __name__ == '__main__':
	pub = rospy.Publisher('commands', Motor, queue_size=1000)
	rospy.init_node('talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	msg = Motor()
	while(True):
		print("Please enter a command in range 0-3")
		cmd=int(input())
		spd=int(input())
		talker(cmd,spd)
