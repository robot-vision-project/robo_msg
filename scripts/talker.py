#!/usr/bin/env python


## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic
import os
import cv2
import numpy as np
import rospy

from robo_msg.msg import Motor
from timeit import default_timer as timer
import time

def talker():
    msg.time =time.time()
    print(msg.time)
    count = 0
    msg.command = count % 3
    msg.speed = 0
    pub.publish(msg)
    # pub2.publish(start)
    rate.sleep()
    count = count + 1

if __name__ == '__main__':
    try:
        yolo_cfg = os.path.abspath('src/robo_msg/scripts/yolov3.cfg')
        yolo_weights = os.path.abspath('src/robo_msg/scripts/yolov3.weights')
        pub = rospy.Publisher('commands', Motor, queue_size=1000)
        # pub2 = rospy.Publisher('time', String, queue_size=1000)
        rospy.init_node('talker', anonymous=True)
        rate = rospy.Rate(10) # 10hz
        msg = Motor()
        net = cv2.dnn.readNet(yolo_weights, yolo_cfg)
        classes = []
        with open("src/robo_msg/scripts/coco.names", "r") as f:
            classes = [line.strip() for line in f.readlines()]

        # print(classes)
        layers_names = net.getLayerNames()
        outputLayers = [layers_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        # Loading the image 
        img = cv2.imread("src/robo_msg/scripts/person.jpeg")
        height, width, channel = img.shape

        # Detecting objects
        blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 255, 0), True, crop=False)
        #for b in blob:
        #    for n, img_blob in enumerate(b):
        #        cv2.imshow(str(n), img_blob)

        net.setInput(blob)
        out = net.forward(outputLayers)
        print(out)

        class_ids = []
        confidence = []
        boxes = []

        for o in out:
            for detection in o:
                scores = detection[5:]
                class_id = np.argmax(scores)
                conf = scores[class_id]
                if conf > 0.5:
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[0] * width)
                    h = int(detection[1] * height)
                    x = int(center_x-w/2)
                    y = int(center_y-h/2)
                    boxes.append([x, y, w, h])
                    confidence.append(float(conf))
                    class_ids.append(class_id)
                

        num_of_obj = len(boxes) 
        for i in range(num_of_obj):
            x, y, w, h = boxes[i]
            label = classes[class_ids[i]]

        print(f"Object Detected is: {label}")

        # cv2.imshow("Image", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        if(label == "tie"):
            talker()
            # end = timer()
            # print(f"Time take to send the message is {end - start} seconds")
            
    except rospy.ROSInterruptException:
        pass
