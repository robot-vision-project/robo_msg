#include <stdio.h>
#include "header.h"

char* translate_log_level(e_log_tag log) {
	switch(log) {
		case LOG_DEBUG:
			return "DEBUG";
		case LOG_WARNING:
			return "WARNING";
		case LOG_ERROR	:
			return "ERROR";
		default:
			return "????";
	}
}

void mcu_debug_printf(e_log_tag tag, int line_num, char* args) {
    printf("%s Line %d: %s", translate_log_level(tag), line_num, args);
}

#define dbg_printf(tag, ...) do { \
        mcu_debug_printf((tag), __LINE__, __VA_ARGS__); \
    } while(0)

#define FIXME() do{ dbg_printf(LOG_ERROR, "FIXME\n"); } while(0)
