typedef enum {
	START,
	LOG_DEBUG = 1,
	LOG_WARNING = 2,
	LOG_ERROR = 3,
	END
} e_log_tag;

char* translate_log_level(e_log_tag log);
